libgtk2-spell-perl (1.04-4) UNRELEASED; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Jan 2016 20:04:32 +0100

libgtk2-spell-perl (1.04-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Change build dependencies on dictionaries.
    myspell-en-us was removed, which leads to a FBTFS bug on the buildds.
    Discovered during the perl 5.22 rebuilds.
  * Mention module name in long description.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Dec 2015 23:33:03 +0100

libgtk2-spell-perl (1.04-2) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Fix grammar error in package description. Thanks to Clayton Casciato
    for the bug report and the patch. (Closes: #687467)
  * Drop xz compression for {binary,source} package, set by default by
    dpkg since 1.17.{0,6}.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Update debian/copyright to copyright-format 1.0
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Thu, 24 Apr 2014 21:42:47 +0200

libgtk2-spell-perl (1.04-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * Bump (build-)dep on libglib-perl to >= 2:1.240.
  * Remove patch fix-Makefile.PL.patch: fixed upstream.
  * Use xz compression for source and binary packages.
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.
  * Bumped Standards-Version to 3.9.2.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 20 Jan 2012 20:10:35 +0100

libgtk2-spell-perl (1.03-5) unstable; urgency=low

  * Added build dependency on myspell-en-us | myspell-dictionary-en |
    hunspell-dictionary-en to fix test failure (closes: #591129).
  * Run tests with LC_ALL=C to prevent the local environment causing test
    failures.
  * Add myself to Uploaders and copyright.
  * Upped Standards-Version to 3.9.1 (no changes).

 -- Chris Butler <chrisb@debian.org>  Sun, 22 Aug 2010 22:50:30 +0100

libgtk2-spell-perl (1.03-4) unstable; urgency=low

  [ Ryan Niebur ]
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * Fix non-numeric dependencies breaking the build with Perl 5.12.
    Thanks to Niko Tyni <ntyni@debian.org> for the patch.
    (Closes: #578906)
    + new patch: fix-Makefile.PL.patch
  * Use source format 3.0 (quilt).
  * Add myself to Uploaders.
  * debian/control: Make build-dep on perl unversioned.
  * Bump Standards-Version to 3.8.4.
  * Use xvfb to run test suite.
  * debian/copyright: Formatting changes for current DEP-5 proposal.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 15 May 2010 12:15:17 +0900

libgtk2-spell-perl (1.03-3) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request
    (http://lists.debian.org/debian-perl/2008/12/msg00163.html)
  * Add debian/watch.
  * Add me to uploaders, remove old maintainer(s)
  * fix watch file
  * debhelper 7
  * policy 3.8.0
  * disable tests, as they spit out error messages
  * machine readable copyright format

 -- Ryan Niebur <ryanryan52@gmail.com>  Sat, 31 Jan 2009 15:01:29 -0800

libgtk2-spell-perl (1.03-2) unstable; urgency=low

  * debian/control: I'm a DD.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Thu, 15 Jul 2004 19:07:34 +0200

libgtk2-spell-perl (1.03-1) unstable; urgency=low

  * Initial release. 

 -- Marc Brockschmidt <marc@dch-faq.de>  Thu, 22 Jan 2004 11:28:53 +0100
